# Python Testing Tutorial

Welcome to the Python Testing Tutorial repository.
The notebook in this repository is designed to be loaded to a Jupyter Lab
instance that has `pytest` and `ipython_pytest` installed.

Prepare a new virtual environment in the root of this repository

```
python3 -m venv venv
. venv/bin/activate
pip install -U pip
pip install pytest ipython_pytest jupyterlab
```

When this is done, launch Jupyter

```
jupyter lab
```

Click on the upload icon, and upload this notebook.
